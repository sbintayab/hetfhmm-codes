Before running the program, please do remember that we use linux/ ubuntu to run the HetFHMM:
First, please compile using following command::::
g++ -c -std=c++0x trans_mat.cpp
g++ -c -std=c++0x observe_mat.cpp
g++ -c -std=c++0x hmm_state.cpp
g++ -c -std=c++0x observ_chain.cpp
g++ -c -std=c++0x fhmm.cpp
g++ -c -std=c++0x main.cpp
g++ -c -std=c++0x gen_data.cpp
g++ -c -std=c++0x genotype.cpp
g++ -c -std=c++0x utils.cpp

Second, create the inference, please following command::::
g++ -o inference main.o utils.o gen_data.o trans_mat.o genotype.o observe_mat.o hmm_state.o observ_chain.o fhmm.o

Third, please run the inference using following command::::
./inference -max_switch "number_of_switch_between_gibbs_sampler_and_EG" -num_of_sets "number_of_dataset" -num_chain "number_of_chain"
Example: ./inference -max_switch 1000 -num_of_sets 1 -num_chain 6



To produce Synthetic data::::
g++ -c -std=c++11 trans_mat.cpp
g++ -c -std=c++11 observe_mat.cpp
g++ -c -std=c++11 hmm_state.cpp
g++ -c -std=c++11 observ_chain.cpp
g++ -c -std=c++11 fhmm.cpp
g++ -c -std=c++11 main.cpp
g++ -c -std=c++11 gen_data.cpp
g++ -c -std=c++11 genotype.cpp
g++ -c -std=c++11 utils.cpp

g++ -o gen_data2 gen_data.o trans_mat.o genotype.o utils.o
rm *.o
./gen_data2 -num_of_sets 4 -file_name input_3e5_s.4.6 -genome_length 300000 -s .4,.6, -max_mut 60 -min_cov 400 -max_cov 500 1> seq_3e5_s.4.6_mincov400_maxcov500

mod, strong, no-300000 weak-1000000


./gen_data2 -num_of_sets 1 -file_name input_5_1_1 -genome_length 300000 -s .06,.09,.2,.15,.5, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_1_1
./gen_data2 -num_of_sets 1 -file_name input_5_1_2 -genome_length 300000 -s .06,.09,.2,.15,.5, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_1_2
./gen_data2 -num_of_sets 1 -file_name input_5_2_1 -genome_length 300000 -s .05,.15,.1,.2,.5, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_2_1
./gen_data2 -num_of_sets 1 -file_name input_5_2_2 -genome_length 300000 -s .05,.15,.1,.2,.5, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_2_2
./gen_data2 -num_of_sets 1 -file_name input_5_3_1 -genome_length 300000 -s .009,.55,.25,.1,.091, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_3_1
./gen_data2 -num_of_sets 1 -file_name input_5_3_2 -genome_length 300000 -s .009,.55,.25,.1,.091, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_3_2
./gen_data2 -num_of_sets 1 -file_name input_5_4_1 -genome_length 300000 -s .01,.17,.5,.07,.25, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_4_1
./gen_data2 -num_of_sets 1 -file_name input_5_4_2 -genome_length 300000 -s .01,.17,.5,.07,.25, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_4_2
./gen_data2 -num_of_sets 1 -file_name input_5_5_1 -genome_length 300000 -s .2,.58,.04,.1,.08, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_5_1
./gen_data2 -num_of_sets 1 -file_name input_5_5_2 -genome_length 300000 -s .2,.58,.04,.1,.08, -max_mut 1000 -min_cov 400 -max_cov 500 1> gold_5_5_200