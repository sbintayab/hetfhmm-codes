#include<cstring>
#include <stdlib.h>
#include <vector>
#include <math.h>
#include<iostream>
#include <fstream>
#include<tr1/random>
#include<random>
#include "trans_mat.h"
#include "genotype.h"
#include <string>
#include <sstream>
//#include <boost/iostreams/tee.hpp>
//#include <boost/iostreams/stream.hpp>

// L : average distance between mutated nucleotide on the genom, come from the trans_mat.h filee

// max_mut: maximum number of clones which can have a mutation on the same nucleotide
// phi: tumor ploidy
// s: the umber clones and their proportion
// sigme: standard deviation of the tumor/normal log-ratio
// outf : name of the output file
// genome_length: maximum length of the genome

using namespace std;

// lambda: is the expected value of the exponential distribution
default_random_engine generator;
double exp_dist(double lambda)
{
    double u = ( rand() / (RAND_MAX+1.0)) ;
    return  - log(1.0 - u) * lambda ;
}

int uniform_int(int min, int max)
{
    return rand() % (max-min+1) + min;
}

//sample a value from a distribution vector.
int sample_dist(vector<double> p)
{
    //partition function
    double Z = 0.0;
    for (int k = 0; k < p.size(); k++)
        Z += p[k];
    // generate result
    double temp = 0.0, u = ( rand() / (RAND_MAX+1.0)) ;
    for (int k = 0; k < p.size(); k++)
    {
        temp += p[k] / Z;
        if (u < temp) return k;
    }
    return 0;
}

//return a value from the given binomial distribution.
int gen_binomial(double mu, int N)
{
    //default_random_engine generator;
    binomial_distribution<int> distribution(N,mu);
    int number = distribution(generator);
    return number; //XXX
}

//return a value from the given normal distribution
double gen_gaussian(double m_t, double sigma)
{
    //default_random_engine generator;
    normal_distribution<double> distribution(m_t,sigma);

    double number = distribution(generator);
    return number; //XXX
}

//set mean value for the normal distribution
double set_mean(vector<double> s, double phi, vector<Genotype> states)
{
    double term1 = 0;
    for (int k = 0; k < s.size(); k++)
        term1 += s[k] * gentype2copynum((Genotype)states[k]);
    double cN = 2;
    return (term1) / (s[0]*cN + (1-s[0]) * phi);
}


//set mu for the binomial distribution based on the model
double set_mu(vector<double> s, vector<Genotype> states)
{
    double term1, term2;
    term1 = term2 = 0;
    for (int k = 0; k < s.size(); k++ )
    {
        term1 += s[k] * gentype2copynum((Genotype)states[k]) * genotype2ratio((Genotype)states[k]);
        term2 += s[k] * gentype2copynum((Genotype)states[k]);
    }
    //
    double rN = .5;
    double cN = 2.0;
    if (term2 != 0)
    {
        return (term1) / (term2);
    }
    else
    {
        return 0;
    }
}

//generate observations based on the binomial and normal distribution
void gen_observed(int &N_t, int &a_t, double &l_t, int min_cov, int max_cov, vector<double> s, double phi, double sigma, vector<Genotype> states)
{
    // generate a_t, N_t
    N_t = uniform_int(min_cov, max_cov);
    double mu_t = set_mu(s, states);
    a_t = gen_binomial(mu_t, N_t); //XXX
    // generate l_t
    double m_t = set_mean(s, phi, states);
    l_t = gen_gaussian(m_t, sigma); // XXX
}


//write data to the files.
void print_data(long t, vector<double> s, double phi, double sigma, vector<Genotype> states,int min_cov, int max_cov, int num_of_sets, string file_name)
{


    for(int i=0; i<num_of_sets; i++)
    {
        stringstream sstm;
        int N_t, a_t;
        double l_t;
        gen_observed(N_t, a_t, l_t, min_cov, max_cov, s, phi, sigma, states);

        sstm << file_name << "_" <<i;
        ofstream myfile (sstm.str(), ios::app );
        if (myfile.is_open())
        {
            myfile<<a_t<<"\t"<<N_t<<"\t"<<l_t<<"\t"<<t<<endl;
            myfile.close();
        }
        else cout << "Unable to open file";
    }



    cout << t << ")  ";
    for (int k = 0; k < s.size() ; k++)
        cout << s[k] << ", ";
    cout << " _ " << phi << " _ " << sigma << " _ ";
    for (int k = 0; k < s.size(); k++)
        cout << genotype2string((Genotype)states[k]) << ", ";
    cout << endl;

}

// s: proportion of different clones
// sN: normal contamination
// max_mut: max number of clones which can have a mutation on the same genome position. for eaveery site, the number of mutated clones is selected uniformly at random from {1,..,max_mut}
// min_cov, max cov: the depth (number of reads) covering each genomic position (in the tumor)
// sigma: the standard deviation of the log depth-ratio
// genome_length: length of the genome
// Note: in this version, max_mut is NOT used to put a maximum on the number of possible mutaions.
// phi: average copy number in tumor, eg it can be 3.5
void gen_data(vector<double> s, double phi, int max_mut, int min_cov, int max_cov, double sigma, long genome_length, int num_of_sets, string file_name)
{

    TransMat tm;
    long t = 0;
    int N_t, a_t;
    double l_t;

    vector<Genotype> states;
    // sample clones genotypes for the first position
    vector<double> unif(21,1.0);
    //cout << "000" << endl;
    states.push_back(gt_AB);
    for (int k = 1; k < s.size(); k++)
        states.push_back((Genotype)(sample_dist(unif)));
    //cout << "111" << endl;

    //write states on the output file XXX
    //cout << " 222 " << endl;
    print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
    //cout << "333" << endl;
    while (true)
    {
        // sample delta, the next genomic mutated position
        long delta = exp_dist(L);
        t += delta;
        if (t >= genome_length) break;
        //cout << "444" << endl;
        // sample the genotypes
        states[0]= gt_AB;   //the normal chain
        for (int k = 1; k < s.size(); k++)
        {
            vector<double> temp = tm.prob_vector((Genotype)states[k],delta);

            states[k] = (Genotype) (sample_dist(temp));
        }
        // if needed to take into account max_mut
        //cout << "555" << endl;
        int sampleMax_mut = max_mut%rand()+1;
        if (max_mut)
        {
            //
        }

        print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
        ////cout << "777" << endl;
    }
}

//maximum number of mutations included
void gen_data2(vector<double> s, double phi, int max_mut, int min_cov, int max_cov, double sigma, long genome_length, int num_of_sets, string file_name)
{

    TransMat tm;
    long t = 0;
    int N_t, a_t;
    double l_t;
    int mut_count[s.size()];

    for (int i=0; i<s.size();i++){
        mut_count[i]=0;
    }

    vector<Genotype> states;
    // sample clones genotypes for the first position
    vector<double> unif(21,1.0);
    //cout << "000" << endl;
    states.push_back(gt_AB);
    for (int k = 1; k < s.size(); k++)
        states.push_back((Genotype)gt_AB);
    //cout << "111" << endl;

    //write states on the output file XXX
    //cout << " 222 " << endl;
    print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
    //cout << "333" << endl;
    while (true)
    {
        // sample delta, the next genomic mutated position
        long delta = exp_dist(L);
        t += delta;
        if (t >= genome_length) break;
        //cout << "444" << endl;
        // sample the genotypes
        states[0]= gt_AB;   //the normal chain
        for (int k = 1; k < s.size(); k++)
        {
            vector<double> temp = tm.prob_vector((Genotype)states[k],delta);
            if (mut_count[k]>max_mut)
            {
                states[k] = (Genotype) (gt_AB);
            }
            else
            {
                states[k] = (Genotype) (sample_dist(temp));
                if (states[k] != gt_AB)
                    mut_count[k]++;
            }

        }
        // if needed to take into account max_mut
        //cout << "555" << endl;
        int sampleMax_mut = max_mut%rand()+1;

        print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
        ////cout << "777" << endl;
    }
}

void gen_data3(vector<double> s, double phi, int max_mut, int min_cov, int max_cov, double sigma, long genome_length, int num_of_sets, string file_name)
{

    TransMat tm;
    long t = 0;
    int N_t, a_t;
    double l_t;
    int mut_count[s.size()];

    for (int i=0; i<s.size();i++){
        mut_count[i]=0;
    }

    vector<Genotype> states;
    // sample clones genotypes for the first position
    vector<double> unif(21,1.0);
    //cout << "000" << endl;
    states.push_back(gt_AB);
    for (int k = 1; k < s.size(); k++)
        states.push_back((Genotype)gt_AB);
    //cout << "111" << endl;

    //write states on the output file XXX
    //cout << " 222 " << endl;
    print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
    //cout << "333" << endl;
    while (true)
    {
        // sample delta, the next genomic mutated position
        long delta = exp_dist(L);
        t += delta;
        if (t >= genome_length) break;
        //cout << "444" << endl;
        // sample the genotypes
        states[0]= gt_AB;   //the normal chain
        for (int k = 1; k < s.size(); k++)
        {
            //vector<double> temp = tm.prob_vector((Genotype)states[k],delta);
            if (mut_count[k]>max_mut)
            {
                states[k] = (Genotype) (gt_AB);
            }
            else
            {
                states[k] = (Genotype) (sample_dist(unif));
                if (states[k] != gt_AB)
                    mut_count[k]++;
            }

        }
        // if needed to take into account max_mut
        //cout << "555" << endl;
        int sampleMax_mut = max_mut%rand()+1;

        print_data(t, s, phi, sigma, states,min_cov, max_cov, num_of_sets,file_name);
        ////cout << "777" << endl;
    }
}
/*
int main(int argc, char* argv[])
{
    srand (time(NULL));

    //   for (int i = 0; i < 10; i++) {
    //      cout << exp_dist(2000) << endl;
    //      cout << uniform_int(1,5) << endl;
    //   }
    vector<double> s;
    s.push_back(.4);
    s.push_back(.6);
    //s.push_back(.2);

    double phi = 3, sigma = 2;
    int min_cov = 30, max_cov = 100, max_mut = 300, num_of_sets=3;
    long genome_length = 100000;
    string file_name = "seq.reference";

// parse the commandline
    for (int i = 1; i < argc; ++i)
    {
        if (std::string(argv[i]) == "-phi")
            phi = atof(argv[++i]);
        if (std::string(argv[i]) == "-sigma")
            sigma = atof(argv[++i]);
        if (std::string(argv[i]) == "-min_cov")
            min_cov = atoi(argv[++i]);
        if (std::string(argv[i]) == "-max_cov")
            max_cov = atoi(argv[++i]);
        if (std::string(argv[i]) == "-max_mut")
            max_mut = atoi(argv[++i]);
        if (std::string(argv[i]) == "-num_of_sets")
            num_of_sets = atoi(argv[++i]);
        if (std::string(argv[i]) == "-genome_length")
            genome_length = atol(argv[++i]);
        if (std::string(argv[i]) == "-file_name")
            file_name = argv[++i];
        if (std::string(argv[i]) == "-s")
        {
            s.clear();
            char *pch = strtok(argv[++i],",");
            while (pch)
            {
                s.push_back(atof(pch));
                pch = strtok(NULL,",");
            }
        }
    }
//   cout << "sN_" << s[0] << "\t" << "s_";
//   for (int k = 0; k < s.size() ; k++)
//      cout << s[k] << k << ",";
//   cout << "\tphi_" << phi << "\tsigma_" << sigma << "\tmin_cov_" << min_cov << "\tmax_cov_" << max_cov << "\tmax_mut_" << max_mut << "\tgenomeLength_" << genome_length << endl;
    //gen_data(s, phi, max_mut, min_cov, max_cov, sigma, genome_length, num_of_sets, file_name);
    gen_data3(s, phi, max_mut, min_cov, max_cov, sigma, genome_length, num_of_sets, file_name);
    return 0;
}
*/
