#ifndef OBSERV_CHAIN_H
#define OBSERV_CHAIN_H

#include <vector>
#include <string>

using namespace std;

struct ObservData {
   long a, N; // alleles matched with reference & total alleles 
   double l;  // "corrected" coverage log-ration, used for copy number
   long loc;  // location on the genome
};

class ObservChain {
public:
   ObservChain();
   ~ObservChain();

   vector<ObservData> data;
   void load_input_file(string fname);
   long size();
};

#endif
