#ifndef GEN_DATA_H
#define GEN_DATA_H
#include <stdlib.h>
#include <vector>
#include <string>
using namespace std;


void gen_data(vector<double> s, double phi, int max_mut, double sigma, long genome_length, string fname);
int sample_dist(vector<double> p);
double set_mu(vector<double> s, vector<Genotype> states);
double set_mean(vector<double> s, double phi, vector<Genotype> states);
double gen_gaussian(double m_t, double sigma);
int gen_binomial(double mu, int N);

#endif
