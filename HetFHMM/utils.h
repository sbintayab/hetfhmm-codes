#ifndef UTILS_H
#define UTILS_H

#include <cmath>
#include <vector>
#include<tr1/random>
#include<random>
#include "genotype.h"
using namespace std;
vector<double> norm_exp(vector<double> logp);
vector<double> norm_normal(vector<double> p);
vector<Genotype> viterbi_to_separate(int num, int num_of_chains);
#endif
