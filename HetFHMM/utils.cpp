#include "utils.h"
using namespace std;

//normalize a given distribution in log
vector<double> norm_exp(vector<double> logp)
{

    vector<double> result;
    double y = logp[0];
    for(int i=0; i!=logp.size();i++)
    {
        if(logp[i]>y)
            y = logp[i];
    }
    double sum_difference=0;
    for(int i=0; i!=logp.size();i++)
    {
        sum_difference += exp(logp[i]-y);
    }

    double log_z = y + log(1 + sum_difference);

    for(int i=0; i!=logp.size();i++)
    {
        result.push_back(exp(logp[i]-log_z));
    }
    return result;
}

//normalize a distribution in normal values
vector<double> norm_normal(vector<double> p)
{
    double sum = 0;
    vector<double> result;
    for(int i=0; i<p.size();i++)
    {
        sum += p[i];
    }

    for(int i=0; i<p.size();i++)
    {
        result.push_back(p[i]/sum);
    }
    return result;
}

//this function separates a number that represents a combined states in Viterbi back to separate states.
vector<Genotype> viterbi_to_separate(int num, int num_of_chains)
{
    vector<Genotype> separated_genotype_reverse;
    vector<Genotype> separated_genotype;

    //keep dividing the number by 21 to separate the combined genotype
    for(int i=0; i!=num_of_chains-1; i++)
    {
        separated_genotype_reverse.push_back((Genotype)(num/21));
        num = num % 21;
    }
    //push back the last genotype
    separated_genotype.push_back((Genotype)(num));

    //reverse the order because the first genotype pushed on is actually the last one
    for(int i=num_of_chains-2; i>=0; i--)
    {
        separated_genotype.push_back(separated_genotype_reverse[i]);
    }
    return separated_genotype;

}
