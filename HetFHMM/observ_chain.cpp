#include "observ_chain.h"
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>

using namespace std;

ObservChain::ObservChain() {
}

ObservChain::~ObservChain() {
}

//split the string by a delimiter
vector<string> &split(const string &s, char delim, std::vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

//split the string by a delimiter
vector<string> split(const std::string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

//load the observations from input files
void ObservChain::load_input_file(string fname) {
	string line;
	fstream myfile;
	myfile.open (fname,ios::in);
	if (myfile.is_open())
	{
		//getline(myfile,line);	//skip the first line
		while (getline(myfile,line))
		{
			//cout << line << endl;
			vector<string> x = split(line, '\t');
			long a = stol((string)x[0]), N=stol((string)x[1]), loc=stol((string)x[3]);

			double l = atof((const char*)(x[2].c_str()));

			ObservData temp;
			temp.a = a;
			temp.N = N;
			temp.l = l;
			temp.loc = loc;
			data.push_back(temp);
		}
		myfile.close();
	}
	else
		cout << "Unable to open file";
}

//return the number length of the input
long ObservChain::size() {
	return data.size();
}
