#ifndef TRANS_MAT_H
#define TRANS_MAT_H

#include "genotype.h"
#include "utils.h"
#include<vector>
#include <cmath>
#include <float.h>

using namespace std;

//ZZZZZZZZ need to change L
const double L=2000; // average length between transitions

class TransMat {
public:
   TransMat(); // called from: hmmstate::constructor()
   ~TransMat();
    double prob_init(Genotype g);
    double prob_trans(Genotype gi, Genotype gj, long delta);
    double log_prob_trans_viterbi(int gi, int gj, long delta, int num_of_chains);
    vector<double> prob_vector(Genotype gi, long delta);
};

#endif
