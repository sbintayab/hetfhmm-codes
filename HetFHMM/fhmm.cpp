#include "fhmm.h"
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>

// class FHMM ==========
FHMM::FHMM(int numc, ObservChains *obsc)
{
    this->observ_chains = obsc;
    for (int k = 0; k < numc; k++)
        add_chain(); // chain[0] is the normal
    p_tmat = new TransMat();
    p_omat = new ObserveMat();

    //initialize S with half tumor half normal. Each clone has uniform distribution
    for(int i=0; i!=numc; i++)
    {
        s.push_back(1.0/(numc));
    }
    phi = 3;
}

void FHMM::em_hard_gibbs()
{//not implemented yet
}

//inference by Viterbi algorithm
void FHMM::Viterbi(vector<double> s,double phi, double sigma, int max_switch, double eta_initial)
{
    for(int num_of_switch=0; num_of_switch<max_switch; num_of_switch++)
    {
        vector< vector<double> > viterbi_table;
        vector< vector<double> > viterbi_back_pointer;
        long  num_of_chains= hmm_chains.size();
        long chain_length=hmm_chains[0].size();

        for(long i=0; i!=chain_length+1; i++)
        {
            vector<double> temp(pow(21,num_of_chains),-10000000);
            viterbi_table.push_back(temp);
            viterbi_back_pointer.push_back(temp);
        }

        vector<double> dummy(pow(21,num_of_chains),1);
        viterbi_table[0] = dummy;
        for(long position=1; position<=chain_length; position++)
        {
            cout<<"current position: "<< position << endl;
            for(long current=0; current<pow(21,num_of_chains); current++)
            {

                //viterbi_table[position][current]=0;
                for(long prev=0; prev<pow(21,num_of_chains); prev++)
                {
                    double pos_t = (*observ_chains)[0]->data[position].loc;
                    double pos_parent = (*observ_chains)[0]->data[position-1].loc;
                    double delta = pos_t - pos_parent;

                    double temp=viterbi_table[position-1][prev]+p_tmat->log_prob_trans_viterbi(prev,current,delta,num_of_chains);
                    vector<Genotype> states = viterbi_to_separate(current,num_of_chains);

                    //need to calculate all P_at and P_lt
                    double P_ats=0, P_lts=0;
                    double P_at, P_lt;
                    //calculate P_at
                    for(int sets=0; sets!=observ_chains->size(); sets++)
                    {
                        long a_t, N_t;
                        double mu;
                        a_t = (*observ_chains)[sets]->data[position].a;
                        N_t = (*observ_chains)[sets]->data[position].N;

                        mu = set_mu(s,states);
                        P_at = p_omat->log_bionomial(a_t,N_t, mu);
                        P_ats += P_at;

                        //calculate P_lt
                        double l_t, m_t;
                        l_t = (*observ_chains)[sets]->data[position].l;
                        m_t = set_mean(s,phi,states);
                        P_lt = p_omat->log_gaussian(l_t,m_t,sigma);
                        P_lts += P_lt;
                    }

                    temp += P_ats + P_lts;


                    if(temp > viterbi_table[position][current])
                    {
                        viterbi_table[position][current] = temp;
                        viterbi_back_pointer[position][current] = prev;
                    }
                }
            }
        }

        vector<long> viterbi_path(chain_length+1,-1);
        double temp=-DBL_MAX;
        for(long state=0; state<pow(21,num_of_chains); state++)
        {
            double test = viterbi_table[chain_length][state];
            if(viterbi_table[chain_length][state]>temp)
            {
                temp=viterbi_table[chain_length][state];
                viterbi_path[chain_length] = state;
            }
        }
        for(long position = chain_length-1; position>0; position--)
            viterbi_path[position] = viterbi_back_pointer[position+1][viterbi_path[position+1]];

        s = EG(s, phi,sigma,eta_initial);

        // output the results
        stringstream sstm;
        sstm << "result_viterbi_"<<max_switch<<"_"<<num_of_switch<<"_"<<eta_initial;
        ofstream myfile (sstm.str());
        if (myfile.is_open())
        {
            for(long position = 0; position<chain_length; position++)
            {
                long combined = viterbi_path[position+1];
                vector<Genotype> current = viterbi_to_separate(combined,num_of_chains);
                myfile << (*(observ_chains))[0]->data[position].loc << ")  ";
                for (int k = 0; k < s.size() ; k++)
                    myfile << s[k] << ", ";
                myfile << " _ " << phi << " _ " << sigma << " _ ";
                for (int k = 0; k < s.size(); k++)
                {
                    myfile << genotype2string(current[k]) << ", ";
                }

                myfile << " _ " << (*(observ_chains))[0]->data[position].N << " _ " << (*(observ_chains))[0]->data[position].a << " _ " << (*(observ_chains))[0]->data[position].l << endl;
            }
            myfile.close();
        }
        else cout << "Unable to open file";
    }

}

//fix sN and s
//inference by gibbs sampling
void FHMM::gibbs(vector<double> s,double phi, double sigma, int gibbs_iter, int max_switch)
{
    //initialize all the genotypes
    vector<double> unif18(20-3+1,1.0);
    for(int i=1; i!=hmm_chains.size(); i++)
    {
        for(int t=0; t!=hmm_chains[i].size(); t++)
        {
            //randomly initialize G with genotypes
            hmm_chains[i][t].state = (Genotype)(sample_dist(unif18)+3);
        }
    }

    for(int num_of_switch=0; num_of_switch<max_switch; num_of_switch++)
    {
        int iter=0;
        while(iter != gibbs_iter)   //this is the convergence criteria
        {

            iter++;
            for(int t=0; t!=hmm_chains[0].size(); t++)  //sample horizontally
            {
                for(int i=1; i!=hmm_chains.size(); i++) //sample vertically
                {
                    double P_at, P_lt;
                    vector<double> P_parent(21,1.0);
                    vector<double> P_child(21,1.0);
                    vector<double> P_posterior(21,1.0);

                    double pos_t = (*observ_chains)[0]->data[t].loc;

                    //calc P_parent
                    if(t-1 >= 0)
                    {
                        double pos_parent = (*observ_chains)[0]->data[t-1].loc;
                        double delta = pos_t - pos_parent;
                        P_parent = p_tmat->prob_vector(hmm_chains[i][t-1].state,delta);
                    }

                    //calculate observations
                    for(int g=0; g<21; g++)
                    {
                        vector<Genotype> states;
                        states.clear();
                        for(int chain=0; chain!=hmm_chains.size(); chain++)
                        {
                            if(chain != i)
                            {
                                states.push_back(hmm_chains[chain][t].state);
                            }
                            else
                            {
                                states.push_back((Genotype)g);
                            }
                        }

                        //need to calculate all P_at and P_lt
                        double P_ats=0, P_lts=0;
                        //calculate P_at
                        for(int sets=0; sets!=observ_chains->size(); sets++)
                        {
                            long a_t, N_t;
                            double mu;
                            a_t = (*observ_chains)[sets]->data[t].a;
                            N_t = (*observ_chains)[sets]->data[t].N;

                            mu = set_mu(s,states);
                            P_at = p_omat->log_bionomial(a_t,N_t, mu);
                            P_ats += P_at;

                            //calculate P_lt
                            double l_t, m_t;
                            l_t = (*observ_chains)[sets]->data[t].l;
                            m_t = set_mean(s,phi,states);
                            P_lt = p_omat->log_gaussian(l_t,m_t,sigma);
                            P_lts += P_lt;
                        }

                        //calc P_child
                        if(t+1 < hmm_chains[0].size())
                        {
                            double pos_child = (*observ_chains)[0]->data[t+1].loc;
                            double delta = pos_child - pos_t;
                            P_child[g] = p_tmat->prob_trans((Genotype)g,hmm_chains[i][t+1].state,delta);
                        }

                        //calc P_posterior
                        double log_parent = log(P_parent[g]);
                        double log_child = log(P_child[g]);
                        if(P_parent[g]==0)
                        {
                            log_parent = -DBL_MAX;
                        }
                        if(P_child[g]==0)
                        {
                            log_child = -DBL_MAX;
                        }
                        P_posterior[g] = log_parent+log_child+P_ats+P_lts;
                    }

                    //normalize and sample from P_posterior
                    vector<double> test = norm_exp(P_posterior);
                    int testIndex = sample_dist(norm_exp(P_posterior));
                    hmm_chains[i][t].state = ((Genotype)testIndex);
                    //cout<<genotype2string(hmm_chains[i][t].state) <<endl;
                    Genotype testG = hmm_chains[i][t].state;
                }
            }
            double likelihood = getLogLikelihood(s,phi,sigma);
            cout<<iter<<","<<likelihood<<endl;

        }

        //switch to EG for s
        s = EG(s, phi,sigma, 0.0003);

        // output the results
        stringstream sstm;
        sstm << "result_"<<max_switch<<"_"<<gibbs_iter<<"_"<<num_of_switch;
        ofstream myfile (sstm.str());
        if (myfile.is_open())
        {
            for(int t=0; t < hmm_chains[0].size(); t++)
            {
                myfile << (*(observ_chains))[0]->data[t].loc << ")  ";
                for (int k = 0; k < s.size() ; k++)
                    myfile << s[k] << ", ";
                myfile << " _ " << phi << " _ " << sigma << " _ ";
                for (int k = 0; k < s.size(); k++)
                {
                    Genotype test = hmm_chains[k][t].state;
                    myfile << genotype2string(hmm_chains[k][t].state) << ", ";
                }

                myfile << " _ " << (*(observ_chains))[0]->data[t].N << " _ " << (*(observ_chains))[0]->data[t].a << " _ " << (*(observ_chains))[0]->data[t].l<<endl;
		//myfile << " _ " << likelihood << endl;
            }
            myfile.close();
        }
        else cout << "Unable to open file";
    }

}

void FHMM::gibbs_independent(vector<double> s,double phi, double sigma, int gibbs_iter, int max_switch)
{
    //initialize all the genotypes
    vector<double> unif18(20-3+1,1.0);
    for(int i=1; i!=hmm_chains.size(); i++)
    {
        for(int t=0; t!=hmm_chains[i].size(); t++)
        {
            //randomly initialize G with genotypes
            hmm_chains[i][t].state = (Genotype)(sample_dist(unif18)+3);
        }
    }

    for(int num_of_switch=0; num_of_switch<max_switch; num_of_switch++)
    {
        int iter=0;
        while(iter != gibbs_iter)   //this is the convergence criteria
        {

            iter++;
            for(int t=0; t!=hmm_chains[0].size(); t++)  //sample horizontally
            {
                for(int i=1; i!=hmm_chains.size(); i++) //sample vertically
                {
                    double P_at, P_lt;
                    vector<double> P_parent(21,1.0);
                    vector<double> P_child(21,1.0);
                    vector<double> P_posterior(21,1.0);

                    double pos_t = (*observ_chains)[0]->data[t].loc;

                    //calc P_parent
                    if(t-1 >= 0)
                    {
                        double pos_parent = (*observ_chains)[0]->data[t-1].loc;
                        double delta = pos_t - pos_parent;
                        P_parent = p_tmat->prob_vector(hmm_chains[i][t-1].state,delta);
                    }

                    //calculate observations
                    for(int g=0; g<21; g++)
                    {
                        vector<Genotype> states;
                        states.clear();
                        for(int chain=0; chain!=hmm_chains.size(); chain++)
                        {
                            if(chain != i)
                            {
                                states.push_back(hmm_chains[chain][t].state);
                            }
                            else
                            {
                                states.push_back((Genotype)g);
                            }
                        }

                        //need to calculate all P_at and P_lt
                        double P_ats=0, P_lts=0;
                        //calculate P_at
                        for(int sets=0; sets!=observ_chains->size(); sets++)
                        {
                            long a_t, N_t;
                            double mu;
                            a_t = (*observ_chains)[sets]->data[t].a;
                            N_t = (*observ_chains)[sets]->data[t].N;

                            mu = set_mu(s,states);
                            P_at = p_omat->log_bionomial(a_t,N_t, mu);
                            P_ats += P_at;

                            //calculate P_lt
                            double l_t, m_t;
                            l_t = (*observ_chains)[sets]->data[t].l;
                            m_t = set_mean(s,phi,states);
                            P_lt = p_omat->log_gaussian(l_t,m_t,sigma);
                            P_lts += P_lt;
                        }

                        P_posterior[g] = P_ats+P_lts;
                    }

                    //normalize and sample from P_posterior
                    vector<double> test = norm_exp(P_posterior);
                    int testIndex = sample_dist(norm_exp(P_posterior));
                    hmm_chains[i][t].state = ((Genotype)testIndex);
                    //cout<<genotype2string(hmm_chains[i][t].state) <<endl;
                    Genotype testG = hmm_chains[i][t].state;
                }
            }
            double likelihood = getLogLikelihood(s,phi,sigma);
            cout<<iter<<","<<likelihood<<endl;

        }

        //switch to EG for s
        s = EG(s, phi,sigma, 0.0003);

        // output the results
        stringstream sstm;
        sstm << "result_"<<max_switch<<"_"<<gibbs_iter<<"_"<<num_of_switch;
        ofstream myfile (sstm.str());
        if (myfile.is_open())
        {
            for(int t=0; t < hmm_chains[0].size(); t++)
            {
                myfile << (*(observ_chains))[0]->data[t].loc << ")  ";
                for (int k = 0; k < s.size() ; k++)
                    myfile << s[k] << ", ";
                myfile << " _ " << phi << " _ " << sigma << " _ ";
                for (int k = 0; k < s.size(); k++)
                {
                    Genotype test = hmm_chains[k][t].state;
                    myfile << genotype2string(hmm_chains[k][t].state) << ", ";
                }

                myfile << " _ " << (*(observ_chains))[0]->data[t].N << " _ " << (*(observ_chains))[0]->data[t].a << " _ " << (*(observ_chains))[0]->data[t].l << endl;
            }
            myfile.close();
        }
        else cout << "Unable to open file";
    }

}

//used by EG algorithm
//returns the gradient of the target function
vector<double> FHMM::getGradF(vector<double> s,double phi, double sigma)
{
    vector<double> GradF(hmm_chains.size(),0);
    for(int t=0; t<hmm_chains[0].size(); t++)
    {
        vector<Genotype> states;
        states.clear();
        //put all the genotype in current position together
        for(int chain=0; chain!=hmm_chains.size(); chain++)
        {
            states.push_back(hmm_chains[chain][t].state);
        }

        long a_t, N_t;
        a_t = (*observ_chains)[0]->data[t].a;
        N_t = (*observ_chains)[0]->data[t].N;
        double l_t = (*observ_chains)[0]->data[t].l;
        double mu_t, m_t;
        mu_t = set_mu(s,states);
        vector<double> grad_mu_t(hmm_chains.size(), 0); //vector with dim=#clones
        m_t = set_mean(s,phi,states);
        vector<double> grad_m_t(hmm_chains.size(), 0); //vector with dim=#clones

        double upper, lower;
        for(int i=0; i<hmm_chains.size(); i++)
        {
            lower+= (s[i]*gentype2copynum((Genotype)states[i])); //\sum_{i=0}^K S_i \cdot c_{t,i}
        }

        for(int k=0; k!=hmm_chains.size(); k++)
        {
            //calculate each \frac{d\mu_t}{dS_k} eqn18
            double r_tk = genotype2ratio((Genotype)states[k]);
            int c_tk = gentype2copynum((Genotype)states[k]);

            //eqn 19

            upper = c_tk*(r_tk - mu_t);

            grad_mu_t[k] = (upper/lower);

            //calculate each \frac{dm_t}{dS_0} eqn21,22
            if(k==0)
            {
                //eqn21 for normal clone
                double upper = gentype2copynum((Genotype)states[0])*(1-m_t);
                double lower = s[0]*gentype2copynum((Genotype)states[0]);
                for(int i=0; i<hmm_chains.size(); i++)
                {
                    lower+= (s[i]*phi); //\sum_{i=0}^K S_i \cdot \phi
                }
                grad_m_t[k]=(upper/lower);
            }
            else
            {
                //eqn22 for tumor clone
                double upper = gentype2copynum((Genotype)states[k])-m_t*phi; //here is the difference
                double lower = s[0]*gentype2copynum((Genotype)states[0]);
                for(int i=1; i<hmm_chains.size(); i++)
                {
                    lower+= (s[i]*phi); //\sum_{i=0}^K S_i \cdot phi
                }
                grad_m_t[k]=(upper/lower);
            }
        }

        for(int sets=0; sets != observ_chains->size(); sets++)
        {
            long a_t, N_t;
            a_t = (*observ_chains)[sets]->data[t].a;
            N_t = (*observ_chains)[sets]->data[t].N;
            double l_t = (*observ_chains)[sets]->data[t].l;


            //put together with chain rule.
            //come back to eqn15
            for(int k=0; k!=hmm_chains.size(); k++)
            {
                //should be -log?
                double buffer = (a_t/mu_t - (N_t-a_t)/(1-mu_t))*grad_mu_t[k]+((l_t-m_t)/pow(sigma,2))*grad_m_t[k];
                GradF[k] -= (a_t/mu_t - (N_t-a_t)/(1-mu_t))*grad_mu_t[k]+((l_t-m_t)/pow(sigma,2))*grad_m_t[k];
            }

        }


    }
    return GradF;
}

//used in EG to check the value of the target function
double FHMM::getF(vector<double> new_s,double phi, double sigma)
{
    double F;
    const double pi = 3.14159265;
    vector<Genotype> states;
    for(int t=0; t<hmm_chains[0].size(); t++)
    {

        states.clear();
        //put all the genotype in current position together
        for(int chain=0; chain!=hmm_chains.size(); chain++)
        {
            states.push_back(hmm_chains[chain][t].state);
        }

        double mu_t, m_t;
        mu_t = set_mu(new_s,states);
        m_t = set_mean(new_s,phi,states);

        for(int sets=0; sets != observ_chains->size(); sets++)
        {
            long a_t, N_t;
            a_t = (*observ_chains)[sets]->data[t].a;
            N_t = (*observ_chains)[sets]->data[t].N;
            double l_t = (*observ_chains)[sets]->data[t].l;


            F -= a_t*log(mu_t)+(N_t - a_t)*log(1-mu_t)-pow(l_t-m_t,2)/(2*pow(sigma,2));

        }

    }
    return F;
}

//return the log likelihood of the whole model
//The log likelihood is supposed to get larger.
double FHMM::getLogLikelihood(vector<double> new_s,double phi, double sigma)
{
    double likelihood;
    const double pi = 3.14159265;
    vector<Genotype> states;
    for(int t=0; t<hmm_chains[0].size(); t++)
    {


        states.clear();
        //put all the genotype in current position together
        for(int chain=0; chain!=hmm_chains.size(); chain++)
        {
            states.push_back(hmm_chains[chain][t].state);
        }

        double mu_t, m_t;

        mu_t = set_mu(new_s,states);
        m_t = set_mean(new_s,phi,states);
        long a_t, N_t;
        for(int sets=0; sets != observ_chains->size(); sets++)
        {

            a_t = (*observ_chains)[sets]->data[t].a;
            N_t = (*observ_chains)[sets]->data[t].N;
            double l_t = (*observ_chains)[sets]->data[t].l;

            likelihood -= a_t*log(mu_t)+(N_t - a_t)*log(1-mu_t)-pow(l_t-m_t,2)/(2*pow(sigma,2));
        }

        //compute transition likelihood
        if(t != 0)
        {
            for(int i=0; i!= hmm_chains.size(); i++)
            {
                //for each clone in one time slot
                Genotype previous = hmm_chains[i][t-1].state;
                Genotype current = hmm_chains[i][t].state;

                double pos_previous = (*observ_chains)[0]->data[t-1].loc;
                double pos_current = (*observ_chains)[0]->data[t].loc;
                double delta = pos_current - pos_previous;
                likelihood -= log(p_tmat->prob_trans(previous,current,delta));
            }

        }

    }
    return likelihood;
}

//Exponentiated Gradient Descent for learning the vector S
vector<double> FHMM::EG(vector<double> s, double phi, double sigma, double eta_initial)
{
    //start with current S
    double tol = 0.0001, iter=1,eta;
    while(true)
    {
        vector<double> GradF;
        GradF = getGradF(s,phi,sigma);  //get the gradient of the target function
        vector<double> new_s(hmm_chains.size(),0);  //initialize the new s vector

        //cout<<iter<<endl;
        eta = eta_initial/iter;
        iter+=0.1;

        //calculate the new s vector
        for(int k=0; k!=hmm_chains.size(); k++)
        {
            double buffer_exp = exp((-eta*GradF[k]));
            new_s[k]= s[k]*buffer_exp;
        }

        //normalize the new S
        new_s = norm_normal(new_s);

        //check if the result is getting better
        double before = getF(s,phi,sigma);
        double after = getF(new_s,phi,sigma);
        double increase = before - after;


        bool finished = true;

        for(int k=0; k!=hmm_chains.size(); k++)
        {
            if(abs(new_s[k]-s[k])>tol)
            {
                finished=false;
                break;
            }
        }
        if(finished)
        {
            return s;
        }

        if(increase >0)
            //return;
            s = new_s;
    }
}


FHMM::~FHMM()
{
    delete p_tmat;
    delete p_omat;
}

//returns the number of chains in the model
int FHMM::size()
{
    return hmm_chains.size();
}

//this method adds a new chain to the model
void FHMM::add_chain()
{
    HMMChain hmmc;
    for (long i = 0; i < (*observ_chains)[0]->size(); i++)
    {
        HMMState hmms;
        hmmc.push_back(hmms);
    }
    hmm_chains.push_back(hmmc);
}
