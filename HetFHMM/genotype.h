#ifndef GENO_TYPE_H
#define GENO_TYPE_H

#include <string>

enum Genotype {gt_NA, gt_A, gt_B,
                      gt_AA, gt_AB, gt_BB,
                      gt_AAA, gt_AAB, gt_ABB, gt_BBB,
                      gt_AAAA, gt_AAAB, gt_AABB, gt_ABBB, gt_BBBB,
                      gt_AAAAA, gt_AAAAB, gt_AAABB, gt_AABBB, gt_ABBBB, gt_BBBBB};

std::string genotype2string(Genotype g);

int gentype2copynum(Genotype g);

double genotype2ratio(Genotype g);

#endif
