#ifndef OBSERVE_MAT_H
#define OBSERVE_MAT_H

class ObserveMat {
public:
   ObserveMat();
   ~ObserveMat();
   double log_bionomial(long a, long N, double p);
   double log_gaussian(double x, double mu, double sigma);

};

#endif


