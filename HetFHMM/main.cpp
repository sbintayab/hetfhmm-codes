#include "fhmm.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

using namespace std;

int main(int argc, char* argv[])
{
    // creating observation_chain to hold and load the input data

    int num_of_sets = 2;    //number of samples that are considered for inference
    ObservChains *pobs_chains = new ObservChains;


    int max_switch = 5;     //number of switches between EG and Gibbs
    int gibbs_iter = 50;     //number of iterations seen as the convergence criteria
    int num_chain = 2;      //number of chains set up for the model
    int start_file_num = 0; //for multiple samples, the start number of the input file
    double sigma = 2;       //the standard deviation used in the model
    double eta_initial=0.03;//the learning rate to start with
    string inference_type = "gibbs";
    string file_name = "input_3e4_s.4.6";

    //read in all the arguments from command line
    for (int i = 1; i < argc; ++i)
    {
        if (std::string(argv[i]) == "-num_of_sets")
            num_of_sets = atoi(argv[++i]);
        if (std::string(argv[i]) == "-gibbs_iter")
            gibbs_iter = atof(argv[++i]);
        if (std::string(argv[i]) == "-max_switch")
            max_switch = atof(argv[++i]);
        if (std::string(argv[i]) == "-num_chain")
            num_chain = atof(argv[++i]);
        if (std::string(argv[i]) == "-eta_initial")
            eta_initial = atof(argv[++i]);
        if (std::string(argv[i]) == "-inference_type")
            inference_type = argv[++i];
        if (std::string(argv[i]) == "-file_name")
            file_name = argv[++i];
        if (std::string(argv[i]) == "-start_file_num")
            start_file_num = atoi(argv[++i]);
    }

    //file name has to be specified
    if(file_name == "")
    {
        cout<<"No input file selected"<<endl;
        exit(1);
    }

    //check for single sample or multiple samples
    if (num_of_sets != 1)
    {
        //read in multiple samples as input
        for(int i=start_file_num; i<num_of_sets+start_file_num; i++)
        {
            ObservChain *ob = new ObservChain;
            stringstream sstm;
            sstm << file_name << "_" <<i;
            ob->load_input_file(sstm.str());
            (*pobs_chains).push_back(ob);
        }
    }
    else
    {
        //only one sample is considered
        ObservChain *ob = new ObservChain;
        stringstream sstm;
        sstm << file_name<<"_0";
        ob->load_input_file(sstm.str());
        (*pobs_chains).push_back(ob);
    }


    // inference for the genotypes and clone sizes
    srand (time(NULL));
    //pfhmm->gibbs(pfhmm->s,pfhmm->phi,sigma);

    // creating factorial hmm

    FHMM *pfhmm = new FHMM(num_chain, pobs_chains);
    pfhmm->phi = 3;

    //start the inference
    if (inference_type == "gibbs")
        pfhmm->gibbs(pfhmm->s,pfhmm->phi,sigma,gibbs_iter,max_switch);
    if (inference_type == "viterbi")
        pfhmm->Viterbi(pfhmm->s,pfhmm->phi,sigma,max_switch,eta_initial);
    if (inference_type == "gibbs_independent")
        pfhmm->gibbs_independent(pfhmm->s,pfhmm->phi,sigma,gibbs_iter,max_switch);

    // kiling the objects
    delete pfhmm;
    delete pobs_chains;

}

