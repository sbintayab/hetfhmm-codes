#ifndef FHMM_H
#define FHMM_H

#include <vector>
#include <cmath>
#include "hmm_state.h"
#include "trans_mat.h"
#include "observe_mat.h"
#include "observ_chain.h"
#include "gen_data.h"
#include "utils.h"
#include <iostream>



using namespace std;

typedef vector<HMMState> HMMChain;
typedef vector<ObservChain*> ObservChains;

class FHMM {
public:
   FHMM(int numc, ObservChains *obsc);
   ~FHMM();
   //===
   void em_hard_gibbs();
   void Viterbi(vector<double> s,double phi, double sigma, int max_switch, double eta_initial);
   void gibbs(vector<double> s,double phi, double sigma, int gibbs_iter, int max_switch);
   void gibbs_independent(vector<double> s,double phi, double sigma, int gibbs_iter, int max_switch);
   vector<double> getGradF(vector<double> s,double phi, double sigma);
   vector<double> EG(vector<double> s, double phi, double sigma, double eta_initial);
   double getF(vector<double> s,double phi, double sigma);
   double getLogLikelihood(vector<double> new_s,double phi, double sigma);

   //===
   vector<HMMChain> hmm_chains;
   vector<double> s; // s_0 is s_N-1 i.e. 1 - normal containation
   ObservChains *observ_chains; // is filled independently
   double phi; //tumor ploidy
   int size(); // returns the number of chains
private:
   void add_chain(); // adds a new chain to the fhmm
   TransMat   *p_tmat; //used to calculate the transition probs
   ObserveMat *p_omat; // used to calculate the obervation probs
};

#endif

