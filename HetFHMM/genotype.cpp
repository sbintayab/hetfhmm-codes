
#include "genotype.h"

std::string genotype2string(Genotype g) {
   std::string geno_string[] = {"NA", "A", "B",
                                 "AA", "AB", "BB",
                                 "AAA", "AAB", "ABB", "BBB",
                                 "AAAA", "AAAB", "AABB", "ABBB", "BBBB",
                                 "AAAAA", "AAAAB", "AAABB", "AABBB", "ABBBB" , "BBBBB"};
   return geno_string[g];
}

//return the copy number of the given genotype
int gentype2copynum(Genotype g) {
   int geno_copy[] = {0, 1, 1,
                       2, 2, 2,
                       3, 3, 3, 3,
                       4, 4, 4, 4, 4,
                       5, 5, 5, 5, 5, 5};
   if ((g <= 20) && (g >= 0)) return geno_copy[g];
   else return 0;
}

//return the B allel ratio of a given genotype
double genotype2ratio(Genotype g) {
   double geno_ratio[] = {0, 1, 0,
                           1, 1/2.0, 0,
                           1, 2/3.0, 1/3.0, 0,
                           1, 3/4.0, 2/4.0, 1/4.0, 0,
                           1, 4/5.0, 3/5.0, 2/5.0, 1/5.0, 0};
   if ((g <= 20) && (g >= 0)) return geno_ratio[g];
   else return 0;
}

