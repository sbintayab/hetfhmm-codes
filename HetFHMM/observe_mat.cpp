#include "observe_mat.h"
#include <math.h>

const double pi = atan(1)*4;

ObserveMat::ObserveMat() {
}

ObserveMat::~ObserveMat() {
}

//return the probability of having a successes among N trials where p is the probability of success in each trial
double ObserveMat::log_bionomial(long a, long N, double p) {
   double res = 0;
   if (p==0)
   {
       return -10000;
   }
   for (long i = 0; i < a; i++)
      res += log(N-i) - log(a-i);
   return res + a * log(p) + (N - a) * log(1-p);
}

//return the probability of sampling x from the gaussian distribution where the mean is mu and the standard deviation is sigma
double ObserveMat::log_gaussian(double x, double mu, double sigma) {
   double z = (x - mu) / sigma;
   return -.5 * z * z - log(sigma) - .5 * log(2*pi);
}



