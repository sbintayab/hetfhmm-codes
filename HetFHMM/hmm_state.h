#ifndef HMM_STATE_H
#define HMM_STATE_H

#include "genotype.h"

class HMMState {
public:
   HMMState();
   ~HMMState();
   Genotype state;
};

#endif
