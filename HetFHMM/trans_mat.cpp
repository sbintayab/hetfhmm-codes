#include "trans_mat.h"


TransMat::TransMat() {
}

TransMat::~TransMat() {

}

//initialize the transition probability by uniform distribution
double TransMat::prob_init(Genotype g) {
   if ((g >= 3) && (g <= 20))
      return 1.0/18;
   else
      return 0;
}

//delta: the nucletide distance between gi and gj
double TransMat::prob_trans(Genotype gi, Genotype gj, long delta) {
   if ((gi < 3) || (gj < 3) || (gi > 20) || (gj > 20)) return 0;
   // define position dependent rho_t
   //delta = fhmm->observ_chain[t].loc - fhmm->observ_chain[t-1].loc;
   double rho = 1.0 - .5 * (1.0 - exp( (-delta)/(2.0*L) ) );
   if (gi == gj)
      return rho;
   else
      return (1.0-rho)/(21-1);
}


//input - two combined states used in Viterbi
//return - transition probability between two combined states
double TransMat::log_prob_trans_viterbi(int gi, int gj, long delta, int num_of_chains)
{
    //convert the combined genotype back to separate genotypes
    vector<Genotype> separated_genotype_i = viterbi_to_separate(gi,num_of_chains);
    vector<Genotype> separated_genotype_j = viterbi_to_separate(gj,num_of_chains);
    double result=0;
    //calculate transition probability for each chain and add all together
    for(int i=0; i!=num_of_chains; i++)
    {
        double temp = prob_trans(separated_genotype_i[i],separated_genotype_j[i],delta);
        if(temp==0)
        {
            result = -DBL_MAX;
            return result;
        }
        result+=log(temp);
    }
    return result;

}

// returns the UNORMALIZED probability vector
vector<double> TransMat::prob_vector(Genotype gi, long delta) {
   vector<double> res;
   double Z = 0.0;
   for (int g = 0; g <= 20; g++) {
      res.push_back(prob_trans(gi, (Genotype) g, delta));
      Z += res[g];
   }
   return res;
}
